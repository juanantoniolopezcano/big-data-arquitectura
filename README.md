# Big Data Arquitectura

Práctica del módulo de Arquitectura del BootCamp Full Stack Big Dat AI y ML VI

# Introducción

La seguridad informática como tal no existe, pero, aunque no haya ninguna técnica que nos garantice la inmunidad de un sistema, podemos contribuir a que éste sea más seguro.

Por este motivo, resulta de interés estudiar ciertos datos que nos permitan encontrar movimientos anómalos en un sistema, accesos no permitidos, así como predecirlos en un futuro para conservar la privacidad de la información y de los usuarios del mismo.

El principal desafío de empresas y compañías es descubrir el uso fraudulento de su software, y poder distinguir a sus empleados y clientes de intrusos que perjudiquen el sistema, ocasionando pérdidas, falta de confianza y, por consiguiente, una mala imagen del negocio. Es por ello, que surge la necesidad de generar perfiles que distingan a los usuarios del sistema de los intrusos. Para este fin, diseño una solución de negocio que satisfaga esta necesidad.

El principal objetivo perseguido ha sido detectar intrusos atendiendo a las características de los datos de cada usuario extraidos de una web. Con estos datos, pretendo crear perfiles que clasifiquen a los tipos de usuarios de una página, o aplicación web, o de un sistema. Teniendo predeterminados ciertos grupos, puedo predecir, con un margen de error, los casos anómalos.

El tipo de análisis considerado es cuantitativo, ya que busco establecer patrones de comportamiento a partir de la recolección de los datos, que aunque no sean todos del mismo tipo, los he unificado en el preprocesado. El diseño de este análisis ha sido la investigación evaluativa.

Cada usuario tiene una forma de interactuar con el ordenador como el clik del ratón, los movimientos a traves de la pantalla, el ancho de la ventana o la velociad al teclear. Por ello mi objetivo es, detectar si el usuario que esta interactuando con la máquina en ese momento es el usuario logado, y sino fuese así que aparezca un mensaje para introducir las credenciales de nuevo.

# Brainstorm

* Para esta práctica, voy a utilizar un dataset propio creado a partir de un código JS, insertado en una página web durante seis meses, son aproximadamente 19900 instancias. 
* Para mi proyecto, utilizares datos en tiempo real.
* El código captura noventa eventos, entre ellos, click del mouse, tamaño pantalla, idUsuario o pixeles de pantalla.
* Una vez capturados se almacenados en una base de datos mySql.
* Creo que Hadoop, Spark ó Splunk vendría bien para mi proyecto.
* Necesito utilizar una tecnología que me permita capturar los datos y procesarlos en tiémpo real.


# Diseño de DAaaS

## Definición de la estrategía del DAaaS

API Detección de intrusos según un modelo predictivo.

## Arquitectura del DAaaS

Se implementa en Google Cloud y se utilizan las siguientes herramientas de GC.

1. Google Cloud SQL ó dataset para almacenar los datos recogidos de cada unsuario, en la aplicación.
2. Google Cloud Trifaca para la limpieza de los datos.
3. Arquitectura Hadoop (MapReduce + Yarn + Hdfs), para dar formato y agrupar los datos.
4. Google Cloud DataStore, almacena los datos agrupados de cada usuario en MongoDB.
5. Dataproc Sapark o Splunk, para el procesamiento de los datos en timepo real.
6. API 24h que lee de Google Cloud DataStore
    
## DAaaS Operating Model Dsing and Rollout

1. Las máquinas de Google Cloud con SQL, Trifaca, Hadoop, DataStore (MongoDB), Dataproc de Sapark o Splunk y el API final funcionaran 24h al día.
2. Proceso de aprendizaje del modelo.
3. Una vez procese los datos de un usuario activo, se dirige al DataStore para comprobar si ese usuario existe y coinciden los datos.
4. Los jobs se automatizan.

## Desarrolllo de la plataforma DAaaS

1. Código JS de extracción de datos: https://drive.google.com/open?id=1a-ifwTYSX6WoPWHn-WLp6_FFDa5gNI4M
2. Dataset en formato .txt: https://drive.google.com/open?id=1q3runH9Hp7eJMoBI55sNc3LQXkjZAglt
3. Dataset en formato .csv: https://drive.google.com/open?id=1UU4Q4bIOXx_CpCNBjgZpc2p72SauwEge
4. El API correra en NodeJS

# Link a Diagrama

Esta en el mismo repositorio de GitLab, por que he sincronizado la app.diagram.net con GitLab, pero si no funcion ase pego el enlace.
https://drive.google.com/file/d/1y8f84DkTiIPlDKl7DwByzYQH2TxX-a5d/view?usp=sharing
